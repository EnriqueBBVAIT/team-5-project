package com.techuniversity.team5project.controllers;

import com.techuniversity.team5project.controllers.interfaces.ContractController;
import com.techuniversity.team5project.model.ContractResponse;
import com.techuniversity.team5project.services.ContractService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api
@RestController
@RequestMapping("/contract")
public class ContractControllerImpl implements ContractController {

    private final ContractService contractService;

    @Autowired
    public ContractControllerImpl(ContractService ContractService) {
        this.contractService = ContractService;
    }


    @GetMapping(value = "/dni")
    public ContractResponse getContract(String dni, String bank) {
        return contractService.getContractResponse(dni, bank);
    }

    /*@GetMapping(value = "/getAll")
    public List<ContractResponse> getAll(){
        return contractService.getContractResponses();
    }*/


/*
    @PutMapping("/put")
    public ContractResponse updateEntity(ContractRequest contractRequest) {
        return contractService.updateContractResponse(contractRequest);
    }

    @DeleteMapping(value = "/{mainId}")
    public void deleteEntity(String mainId) {
        contractService.deleteContractResponse(mainId);
    }

    @PostMapping
    public ContractResponse createEntity(ContractRequest contractRequest){
        return contractService.createMain(contractRequest);

    }*/
}
