package com.techuniversity.team5project.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClientResponse {

    private String name;
    private String surname;
    private String address;
    private int phoneNumber;
    private String cclient;


}
