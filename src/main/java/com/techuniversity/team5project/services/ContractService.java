package com.techuniversity.team5project.services;

import com.techuniversity.team5project.domain.entities.Account;
import com.techuniversity.team5project.domain.entities.Client;
import com.techuniversity.team5project.model.AccountRequest;
import com.techuniversity.team5project.model.ClientRequest;
import com.techuniversity.team5project.model.ClientResponse;
import com.techuniversity.team5project.model.ContractResponse;
import com.techuniversity.team5project.repositories.ClientRepository;
import com.techuniversity.team5project.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContractService {
    private final ClientRepository clientRepository;
    private final AccountRepository accountRepository;

    @Autowired
    public ContractService(ClientRepository clientRepository, AccountRepository accountRepository) {
        this.clientRepository = clientRepository;
        this.accountRepository = accountRepository;
    }

    public ContractResponse getContractResponse(String dni, String bank) {
        Client client = clientRepository.findByDniAndBank(dni, bank);
        Account account = accountRepository.findByCclientAndBank(client.getCclient(), bank);
        return ContractResponse.builder()
                .name(client.getName())
                .surname(client.getSurname())
                .address(client.getAddress())
                .phoneNumber(client.getPhoneNumber())
                .bank(account.getBank())
                .counterpart(account.getCounterpart())
                .branch(account.getBranch())
                .sheet(account.getSheet())
                .iban(account.getIban()).build();
    }


    /*public ClientResponse getClientResponse(ClientRequest clientRequest, AccountRequest accountRequest) {
        Client client = clientRepository.findByDniAndBank(clientRequest.getDni(), clientRequest.getBank());
        Account account = accountRepository.findByCclientAndBank(accountRequest.)
        return mapClientToResponse(client);
    }

    private ContractResponse mapToResponse(Client client, Account account) {


    }*/
    /*private ContractResponse mapToResponse(ClientRequest clientRequest, AccountRequest accountRequest) {
        return ClientResponse
                .builder()
                .name(client.getName())
                .surname(client.getSurname())
                .address(client.getAddress())
                .phoneNumber(client.getPhoneNumber())
                .cclient(client.getCclient()).build();

    }*/
}
