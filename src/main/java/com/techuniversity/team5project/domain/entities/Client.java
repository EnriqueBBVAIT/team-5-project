package com.techuniversity.team5project.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "Client")
public class Client {

    private String dni;
    private String bank;
    private String name;
    private String surname;
    private String address;
    private int phoneNumber;
    private String cclient;
    //private String iban;
}
