package com.techuniversity.team5project.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "Account")
public class Account {

    private String cclient;
    private String bank;
    private String branch;
    private String counterpart;
    private String sheet;
    private String iban;
}
