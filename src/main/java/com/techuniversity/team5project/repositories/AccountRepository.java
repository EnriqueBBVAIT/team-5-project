package com.techuniversity.team5project.repositories;

import com.techuniversity.team5project.domain.entities.Account;
import com.techuniversity.team5project.domain.entities.Client;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccountRepository extends MongoRepository<Account, String> {

    Account findByCclientAndBank(String cclient, String bank);
}
